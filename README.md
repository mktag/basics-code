# README #

//Base Class
public class Base {

	public void showClass() {
		System.out.println("This is Base");
	}
	


//Extended CLass
class Derived extends Base {

	public void showClass() {
		System.out.println("This is Derived");
	}


//Test extended
public class TestExtend {

	public static void main(String[] args) {
		Base base = new Derived();
		base.showClass();
		
		Base base1 = new Base();
		base1.showClass();
	}
	

